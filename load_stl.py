import os.path
from stl import mesh
import gc

"""
Creates an exnformat file that can be loaded by Zinc to mesh.
"""


def convertSTL(filename, save_name):
    """
    This loops loads in the STL file and writes out an exformat (maybe into memory) to be read into zinc.

    :param filename:
    :return:
    """

    save_name = os.path.splitext(save_name)[0]

    my_mesh = mesh.Mesh.from_file(filename, mode=mesh.stl.ASCII)
    SURFACE_STRING = ""
    NODE_STRING = ""
    LINE_STRING = ""

    # The pieces are the strings that will be filled and added to the STRINGS
    node_piece = " Node: {}\n" \
                 " {: .15e}\n" \
                 " {: .15e}\n" \
                 " {: .15e}\n"

    line_piece = " Element: 0 0 {}\n"

    surface_piece = " Element: {} 0 0\n" \
                    " Faces:\n" \
                    " 0 0 {}\n" \
                    " 0 0 {}\n" \
                    " 0 0 {}\n" \
                    " Nodes:\n" \
                    " {} {} {}\n"

    # Three files are opened so the output is generated as it is converted
    with open('{}.exline'.format(save_name), mode='w') as line_output:
        with open('{}.exsurface'.format(save_name), mode='w') as surface_output:
            with open('{}.exnode'.format(save_name), mode='w') as node_output:

                # Start of the file and nodes
                node_output.write("Region: /\n "
                                  "!#nodeset nodes\n "
                                  "#Fields=1\n"
                                  " 1) coordinates, coordinate, rectangular cartesian, #Components=3\n"
                                  "  x.  Value index=1, #Derivatives=0, #Versions=1\n"
                                  "  y.  Value index=2, #Derivatives=0, #Versions=1\n"
                                  "  z.  Value index=3, #Derivatives=0, #Versions=1\n")

                line_output.write("Region: /\n"
                                  " Shape. Dimension=1, line\n"
                                  " #Scale factor sets=0\n"
                                  " #Nodes=0\n"
                                  " #Fields=0\n")

                surface_output.write("Region: /\n"
                                     " Shape. Dimension=2, simplex(2)*simplex\n"
                                     " #Scale factor sets=0\n"
                                     " #Nodes=3\n"
                                     " #Fields=1\n"
                                     " 1) coordinates, coordinate, rectangular cartesian, #Components=3\n"
                                     " x. l.simplex(2)*l.simplex, no modify, standard node based.\n"
                                     "  #Nodes=3\n"
                                     "  1. #Values=1\n"
                                     "   Value labels: value\n"
                                     "   Scale factor indices: 0\n"
                                     "  2. #Values=1\n"
                                     "   Value labels: value\n"
                                     "   Scale factor indices: 0\n"
                                     "  3. #Values=1\n"
                                     "   Value labels: value\n"
                                     "   Scale factor indices: 0\n"
                                     " y. l.simplex(2)*l.simplex, no modify, standard node based.\n"
                                     "   #Nodes=3\n"
                                     "   1. #Values=1\n"
                                     "    Value labels: value\n"
                                     "    Scale factor indices: 0\n"
                                     "   2. #Values=1\n"
                                     "    Value labels: value\n"
                                     "    Scale factor indices: 0\n"
                                     "   3. #Values=1\n"
                                     "    Value labels: value\n"
                                     "    Scale factor indices: 0\n"
                                     " z. l.simplex(2)*l.simplex, no modify, standard node based.\n"
                                     "  #Nodes=3\n"
                                     "  1. #Values=1\n"
                                     "    Value labels: value\n"
                                     "    Scale factor indices: 0\n"
                                     "  2. #Values=1\n"
                                     "    Value labels: value\n"
                                     "    Scale factor indices: 0\n"
                                     "  3. #Values=1\n"
                                     "    Value labels: value\n"
                                     "    Scale factor indices: 0\n")

                # Dictionaries that hash the coordinates of the nodes. Checks if the key exists already and if so finds
                # the number that it corresponds to. The line list checks if the line list exists and if so adds and
                # removes from the dictionary
                Lines = {}
                Nodes = {}
                nodes = [0] * 3
                face_lines = [0] * 3

                node_access = {}

                node_counter = 0
                line_counter = 0

                # For each surface in the mesh!
                for surface_counter in range(len(my_mesh)):
                    element = my_mesh.points[surface_counter]

                    # Loop through the nodes and see if they  already exist
                    for n in range(3):

                        key = tuple(element[3 * n: 3 * (n+1)])

                        # Nodes stores all the nodes and their number in a dictionary
                        # Number is the node number. It's looked up
                        if key in Nodes.iterkeys():
                            number = Nodes[key]
                            node_access[number] += 1
                            if node_access[number] > 4:
                                Nodes.pop(key, None)
                                node_access.pop(number, None)
                        # Else it adds the node to the file
                        # Adds to the Node_Access dictionary
                        else:
                            node_counter += 1
                            Nodes[key] = node_counter
                            number = node_counter
                            node_access[number] = 0
                            NODE_STRING += node_piece.format(number, *key)

                        # Adds the node to the dictionary
                        nodes[n] = number

                    # Creates the three line types
                    line_node_sets = [(nodes[0], nodes[1]), (nodes[0], nodes[2]), (nodes[1], nodes[2])]

                    # Again generate the three possible lines
                    # If it already exists, look it up and remove it
                    for l, line_key in enumerate(line_node_sets):
                        if line_key in Lines.iterkeys():
                            line_num = Lines.pop(line_key, None)
                        else:
                            line_counter += 1
                            Lines[line_key] = line_counter
                            line_num = line_counter
                            LINE_STRING += line_piece.format(line_num, *nodes)

                        # Sets the line number
                        face_lines[l] = line_num

                    SURFACE_STRING += surface_piece.format(surface_counter + 1, *(face_lines + nodes))

                    # Checks if it should be written to file
                    if (surface_counter + 1) % 500 == 0:
                        node_output.write(NODE_STRING)
                        NODE_STRING = ""
                        line_output.write(LINE_STRING)
                        LINE_STRING = ""
                        surface_output.write(SURFACE_STRING)
                        SURFACE_STRING = ""
                        print "Processed {:,}/{:,} {:.3%}".format(surface_counter + 1, len(my_mesh), (surface_counter + 1) / float(len(my_mesh)))

                node_output.write(NODE_STRING)
                line_output.write(LINE_STRING)
                surface_output.write(SURFACE_STRING)
                print "Processed all {} elements.".format( len(my_mesh))

    # Append the line file with the surface file
    with open(save_name + ".exelem", mode='w') as element_file:
        for fname in [save_name + ".exline", save_name + ".exsurface"]:
            with open(fname, mode='r') as infile:
                for line in infile:
                    element_file.write(line)

    # Remove the two temporary files
    os.remove(save_name + ".exline")
    os.remove(save_name + ".exsurface")

    print "All done... exiting"
