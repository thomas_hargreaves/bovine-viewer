import os.path

from PySide import QtCore, QtGui
from opencmiss.zinc.field import Field
from opencmiss.zinc.graphics import Graphics
from opencmiss.zinc.material import Material
from opencmiss.zinc.status import *

from musculature import Musculature
from node import Nodes
from planes import Planes
from viewer_ui import Ui_Widget
from zinc.zinc_helper import *
from zinc.zinc_interactive import ZincInteractiveTool
from zinc.zinc_loader import LoadExfile


class ViewerWidget(QtGui.QWidget):
    """
    This class is a the viewer window. It takes the UI from another file. It stores the location of the sceneviewer
    widget so that it can be passed up to the main program if required.
    Helper classes will act on this sceneviewer.
    Any changes that need to be fixed from the generated file will be applied in this
    """

    def __init__(self, context):
        """
        Sets the context and the UI for the GUI up.
        :param context: The OpenCMISS zinc context that is passed down to the widgets that need it.
        :return: None
        """
        super(ViewerWidget, self).__init__()

        self._ui = None
        self._ui = Ui_Widget()
        self._ui.setupUi(self)
        self._context = context
        self._sceneviewer = self._ui.sceneviewerWidget
        self._sceneviewer.setContext(context)
        self._tool = None

        self._node = Nodes(context, self._sceneviewer)

        # Button Groups
        self._ui.colour_group = QtGui.QButtonGroup()
        self._ui.visibility_group = QtGui.QButtonGroup()
        self._ui.visibility_group.setExclusive(False)
        self._ui.planeGroup.setExclusive(False)

        self._muscles = Musculature(context, self._sceneviewer)
        self._muscles.setListWidget(self._ui.muscle_list)
        self._muscles.setButtonGroups(self._ui.colour_group, self._ui.visibility_group)

        self._planes = Planes(context)

        self.setUpConnections()

        self._edit_buttons = [
            self._ui.create_button,
            self._ui.edit_button,
            self._ui.deselect_button,
            self._ui.delete_button,
            self._ui.muscle_rename,
            self._ui.delete_button,
            self._ui.saveButton,
            self._ui.muscle_remove,
        ]

        self._edit = False

        self._no_plane_buttons = [
            self._ui.planeSilder,
        ]

        self._no_plane = False
        self._position = [50] * 4

        self._save_filter_dialog = {
            # "EX_ALL_ONE": "Everything as one EX File (*.exformat)",
            "EX_ALL_TWO": "Everything (including skeleton) (*.exnode *.exelem)",
            # "EX_NODES": "Muscle Nodes (*.exnode)",
            # "EX_ELEMENTS": "Muscle Elements (*.exelem)",
            # "EX_BOTH_ONE": "Muscle Nodes and Elements (*.exformat)",
            "EX_MUSCLE_TWO": "Just the muscles as individual files (*.exnode *.exelem)"
        }

    def setUpConnections(self):
        """
        :return:
        """
        self._sceneviewer.graphicsInitialized.connect(self.createGraphics)
        self._sceneviewer.graphicGroup.connect(self._node.createGroupNodeGraphics)
        self._ui.viewAllButton.clicked.connect(self._sceneviewer.viewAll)
        self._ui.saveButton.clicked.connect(self.save)

        ui = self._ui
        # Node Connections
        node = self._node
        ui.programMode.buttonClicked.connect(self.setProgramMode)

        # Muscle Connections
        muscle = self._muscles
        ui.muscleGroup.buttonClicked.connect(muscle.processMuscleButton)
        self._muscles.check_edit_state.connect(self.enableEditing)

        # Plane
        self._ui.planeGroup.buttonClicked.connect(self.setPlane)
        # self._ui.planeSilder.sliderMoved.connect(self.movePane)
        self._ui.planeSilder.valueChanged.connect(self.movePane)
        self._planes.changeCoincidentGraphics.connect(self._node.changeDepthField)
        self._planes.updatePlaneDepth.connect(self._node.updateDepth)
        self._planes.updatePlaneDepth.connect(self.updateLabel)

    def updateLabel(self, number):
        self._ui.planeIndicator.setText('{: >7.3}'.format(number))

    def save(self):
        region = self._context.getDefaultRegion()

        # Get the save location and name
        name, selected_filter= QtGui.QFileDialog.getSaveFileName(self,
                                                                 caption="Save",
                                                                 filter=";;".join(self._save_filter_dialog.itervalues()))

        if selected_filter is not None and name is not None:

            stream_information = region.createStreaminformationRegion()
            directory = os.path.dirname(name)
            name = os.path.splitext(os.path.basename(name))[0]

            # TODO check out the Created_Nodes file
            if selected_filter == self._save_filter_dialog["EX_ALL_TWO"]:
                stream_file = stream_information.createStreamresourceFile('{}.exnode'.format(name))
                stream_information.setResourceDomainTypes(stream_file, Field.DOMAIN_TYPE_NODES)
                stream_file = stream_information.createStreamresourceFile('{}.exelem'.format(name))
                stream_information.setResourceDomainTypes(stream_file, Field.DOMAIN_TYPE_MESH1D | Field.DOMAIN_TYPE_MESH2D)

            # elif selected_filter == self._save_filter_dialog["EX_ALL_ONE"]:
            #     stream_information.createStreamresourceFile('{}'.format(name))

            elif selected_filter == self._save_filter_dialog["EX_MUSCLE_TWO"]:
                # Saves all the muscles to the file

                for muscle_name in self._muscles._muscleNames:
                    nodesetGroup = self._context.getDefaultRegion().getFieldmodule().findNodesetByName(
                        "{}.nodes".format(Nodes.GROUP_NAME))
                    if nodesetGroup.getSize() > 0:
                        name_no_ext = os.path.join(directory, muscle_name)
                        stream_file = stream_information.createStreamresourceFile('{}.exnode'.format(name_no_ext))
                        stream_information.setResourceDomainTypes(stream_file, Field.DOMAIN_TYPE_NODES)
                        stream_information.setResourceGroupName(stream_file, muscle_name)
                        stream_file = stream_information.createStreamresourceFile('{}.exelem'.format(name_no_ext))
                        stream_information.setResourceDomainTypes(stream_file,
                                                                  Field.DOMAIN_TYPE_MESH1D | Field.DOMAIN_TYPE_MESH2D)
                        stream_information.setResourceGroupName(stream_file, muscle_name)

                # Also save the excess created nodes
                nodesetGroup = self._context.getDefaultRegion().getFieldmodule().findNodesetByName("{}.nodes".format(Nodes.GROUP_NAME))
                if nodesetGroup.isValid() and nodesetGroup.getSize() > 0:
                    filename = str(os.path.join(directory, Nodes.FILENAME))
                    stream_file = stream_information.createStreamresourceFile(filename)
                    stream_information.setResourceDomainTypes(stream_file, Field.DOMAIN_TYPE_NODES)
                    stream_information.setResourceGroupName(stream_file, Nodes.GROUP_NAME)

            status = region.write(stream_information)
            if status == OK:
                QtGui.QMessageBox.about(self, "Save file status", "Save successful")
            else:
                QtGui.QMessageBox.about(self, "Save file status", "Error saving files")

    @QtCore.Slot(QtGui.QAbstractButton)
    def setPlane(self, button):

        name = button.objectName()
        position = 3
        status = False

        # Unchecks all the buttons except the pressed one
        # Also saves the location of the current checked button.
        for n, button in enumerate(self._ui.planeGroup.buttons()):
            if button.objectName() == name:
                position = n
            else:
                if button.isChecked():
                    self._position[n] = self._ui.planeSilder.value()
                button.setChecked(False)

        if name == "planeX":
            self._planes.changePlane(0)
            status = True
        elif name == "planeY":
            self._planes.changePlane(1)
            status = True
        elif name == "planeZ":
            self._planes.changePlane(2)
            status = True
        elif name == "noPlane":
            self._planes.changePlane(3)
            status = False
        else:
            raise AttributeError("Button {} doesn't exist".format(name))

        self._no_plane = status

        for button in self._no_plane_buttons:
            button.setEnabled(status)

        if not self._edit and status:
            for button in self._edit_buttons:
                button.setEnabled(False)

        self._ui.planeSilder.setValue(self._position[position])

    @QtCore.Slot(int)
    def movePane(self, pos):

        pos /= float(100)
        self._planes.moveCurrentPlane(pos)

    @QtCore.Slot(int)
    def moveWheelPane(self, pos):

        value = self._ui.planeSilder.value()
        pos += value
        self._ui.planeSilder.setSliderPosition(pos)
        self.movePane(pos)

    @QtCore.Slot(QtGui.QAbstractButton)
    def setProgramMode(self, button):

        name = button.objectName()
        if name == "create_button":
            self._sceneviewer.setSelectionMode("CREATE")
        elif name == "edit_button":
            self._sceneviewer.setSelectionMode("EDIT")
        elif name == "view_button":
            self._sceneviewer.setSelectionMode("VIEW")

    @QtCore.Slot()
    def enableEditing(self, state):

        check = self._ui.create_button.isEnabled()
        if check == state:
            return

        self._edit = state
        for button in self._edit_buttons:
            button.setEnabled(state)

        if state:
            state = self._no_plane
        else:
            state = False

        for button in self._no_plane_buttons:
            button.setEnabled(state)

    @QtCore.Slot()
    def createGraphics(self):
        """
        This function is called when the GL is first initialised.
        All files are to be selected. The following are reserved names:
        - 
        :return:
        """

        # TODO implement logic that involves error messages
        skeleton_files, selected_filter = QtGui.QFileDialog.getOpenFileNames(
            self,
            "Please select the skeleton files",
            filter="exfiles (*.ex*);; All files (*.*)")

        # Logic for trying again
        if not(skeleton_files and selected_filter):
            self.close()

        skin_files, selected_filter = QtGui.QFileDialog.getOpenFileNames(
            self,
            "OPTIONAL: Please select the files for the skin",
            filter="exfiles (*.ex*);; All files (*.*)")

        muscle_files, selected_filter = QtGui.QFileDialog.getOpenFileNames(
                self,
                "OPTIONAL: Please select the precreated muscles",
                filter="exfiles (*.ex*);; All files (*.*)")

        # Load the files
        current_file = 0
        num_files = len(skeleton_files) + len(skin_files) + len(muscle_files)
        progress = QtGui.QProgressDialog("Loading files...", "Abort", 0, num_files, self)
        progress.setWindowModality(QtCore.Qt.WindowModal)
        progress.setMinimumDuration(0)
        progress.setValue(current_file)

        self.loadSkeleton(skeleton_files)
        current_file += len(skeleton_files)
        progress.setValue(current_file)

        self.setupTool()
        self.createSkeletonGraphics()
        if skin_files:
            self.createSkinGraphics(skin_files)
            current_file += len(skin_files)
            progress.setValue(current_file)
        if skin_files:
            self.loadMuscles(muscle_files)
            current_file += len(skin_files)
            progress.setValue(current_file)

        if progress.wasCanceled():
            self.close()

        self._planes.createFiniteElements()
        self._node.createNodeGraphics()
        self._tool.setPlaneElement(self._planes.getPlaneElement())
        progress.reset()

    def loadMuscles(self, muscle_files):
        """Load in the files and create muscle graphics"""

        muscle_region = self._context.getDefaultRegion()

        created_node_file = [file for file in muscle_files if os.path.split(file)[1] == Nodes.FILENAME]
        if len(created_node_file):
            created_node_file = created_node_file[0]
            LoadExfile(muscle_region, created_node_file).loadFile(fieldGroupName=Nodes.GROUP_NAME)
            muscle_files.remove(created_node_file)

        muscles = {}

        for muscle_file in muscle_files:
            name = str(os.path.splitext(os.path.split(muscle_file)[1])[0])

            if name in muscles.iterkeys():
                muscles[name].append(muscle_file)
            else:
                muscles[name] = [muscle_file]

        counter = 0

        for name, muscle_file in muscles.iteritems():
            LoadExfile(muscle_region, muscle_file).loadFile(fieldGroupName=name)
            counter += 1
            self._muscles.addMuscleRow(counter, 0, name)

    def createSkinGraphics(self, skin_files):
        """ From skin files it creates a new region and new graphics"""

        region = self._context.getDefaultRegion().createChild('skin')
        LoadExfile(region, skin_files).loadFile()

        # region = self._context.getDefaultRegion().createChild('skin')
        # stream_information = region.createStreaminformationRegion()
        # for file in skin_files:
        #     stream_information.createStreamresourceFile(str(file))
        # region.read(stream_information)

        with get_materialmodule(region) as materialmodule:
            skin_material = materialmodule.findMaterialByName('tissue')
            skin_material.setAttributeReal(Material.ATTRIBUTE_ALPHA, 0.7)

        with get_fieldmodule(region) as fieldmodule:
            finite_element_field = fieldmodule.findFieldByName('coordinates')

        with get_scene(region) as scene:
            surface = scene.createGraphicsSurfaces()
            surface.setCoordinateField(finite_element_field)
            surface.setSelectMode(Graphics.SELECT_MODE_OFF)
            surface.setMaterial(skin_material)

        # Add the extra
        button = QtGui.QCheckBox('Show outer skin')
        button.stateChanged.connect(self.toggleSkin)
        button.setChecked(True)
        self._ui.sideFrame.layout().insertWidget(4, button)

    @QtCore.Slot(bool)
    def toggleSkin(self, state):

        region = self._context.getDefaultRegion().findChildByName('skin')
        with get_scene(region) as scene:
            scene.setVisibilityFlag(state)

    def loadSkeleton(self, cow_files):
        """
        Loads in the files.
        :return: None
        """

        LoadExfile(self._context.getDefaultRegion().createChild('skeleton'), cow_files).loadFile()

        # region = self._context.getDefaultRegion().createChild('skeleton')
        # stream_file = region.createStreaminformationRegion()
        #
        # for cow_file in cow_files:
        #     stream_file.createStreamresourceFile(str(cow_file))
        # region.read(stream_file)

        # Create a skeleton group and add the whole input into it.
        # with get_fieldmodule(region) as fieldmodule:
        #     skeleton_group = fieldmodule.createFieldGroup()
        #     one_field = fieldmodule.createFieldConstant(1.0)
        #     skeleton_group.setManaged(True)
        #     skeleton_group.setName('skeleton')
        #
        #     nodeset = fieldmodule.findNodesetByFieldDomainType(Field.DOMAIN_TYPE_NODES)
        #     skeleton_group.createFieldNodeGroup(nodeset).getNodesetGroup().addNodesConditional(one_field)
        #
        #     mesh_1 = fieldmodule.findMeshByDimension(1)
        #     skeleton_group.createFieldElementGroup(mesh_1).getMeshGroup().addElementsConditional(one_field)
        #
        #     mesh_2 = fieldmodule.findMeshByDimension(2)
        #     skeleton_group.createFieldElementGroup(mesh_2).getMeshGroup().addElementsConditional(one_field)

    def createSkeletonGraphics(self):
        """
        Creates graphics for the skeleton model
        :return: None
        """

        region = self._context.getDefaultRegion().findChildByName('skeleton')

        with get_materialmodule(region) as material_module:
            bone_material = material_module.findMaterialByName('white')

        with get_fieldmodule(region) as fieldmodule:
            finite_element_field = fieldmodule.findFieldByName('coordinates')
            # group = fieldmodule.findFieldByName('skeleton')

        with get_scene(region) as scene:
            surfaces = scene.createGraphicsSurfaces()
            surfaces.setName('skeleton')
            surfaces.setCoordinateField(finite_element_field)
            surfaces.setMaterial(bone_material)
            surfaces.setSelectMode(Graphics.SELECT_MODE_OFF)
            # if group.isValid():
                # surfaces.setSubgroupField(group)

            # Lower the graphics
            tessellation = surfaces.getTessellation()
            tessellation.setMinimumDivisions(1)
            tessellation.setCircleDivisions(3)
            tessellation.setRefinementFactors(1)

    def setupTool(self):
        region = self._context.getDefaultRegion()

        with get_fieldmodule(region) as fieldmodule:
            finite_element_field = fieldmodule.findFieldByName('coordinates')
            # Sets the node create coordinate field

        self._tool = ZincInteractiveTool()
        self._sceneviewer.setTool(self._tool)
        self._node.setTool(self._tool)

        # Connections
        self._ui.delete_button.clicked.connect(self._tool.deleteSelectedNodesAndElements)
        self._muscles.muscle_name.connect(self._tool.setMuscleGroup)
        self._ui.deselect_button.clicked.connect(self._tool.deselect)
        self._sceneviewer.mouseMoved.connect(self.moveWheelPane)

