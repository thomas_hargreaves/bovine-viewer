"""
Zinc Sceneviewer Widget

Implements a Zinc Sceneviewer Widget on Python using PySide or PyQt,
which renders the Zinc Scene with OpenGL and allows interactive
transformation of the view.
Widget is derived from QtOpenGL.QGLWidget.

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.
"""
# This python module is intended to facilitate users creating their own applications that use OpenCMISS-Zinc
# See the examples at https://svn.physiomeproject.org/svn/cmiss/zinc/bindings/trunk/python/ for further
# information.

try:
    from PySide import QtCore, QtOpenGL
except ImportError:
    from PyQt4 import QtCore, QtOpenGL
    QtCore.Signal = QtCore.pyqtSignal
    QtCore.Slot = QtCore.pyqtSlot

from opencmiss.zinc.field import Field
from opencmiss.zinc.glyph import Glyph
from opencmiss.zinc.scenecoordinatesystem import \
    SCENECOORDINATESYSTEM_WINDOW_PIXEL_TOP_LEFT
from opencmiss.zinc.sceneviewer import Sceneviewer, Sceneviewerevent
from opencmiss.zinc.sceneviewerinput import Sceneviewerinput
from opencmiss.zinc.status import OK

# mapping from qt to zinc start
# Create a button map of Qt mouse buttons to Zinc input buttons
button_map = {QtCore.Qt.LeftButton: Sceneviewerinput.BUTTON_TYPE_LEFT,
              QtCore.Qt.MidButton: Sceneviewerinput.BUTTON_TYPE_MIDDLE,
              QtCore.Qt.RightButton: Sceneviewerinput.BUTTON_TYPE_RIGHT}


# Create a modifier map of Qt modifier keys to Zinc modifier keys
def modifier_map(qt_modifiers):
    """
    Return a Zinc Sceneviewerinput modifiers object that is created from
    the Qt modifier flags passed in.
    """
    modifiers = Sceneviewerinput.MODIFIER_FLAG_NONE
    if qt_modifiers & QtCore.Qt.SHIFT:
        modifiers = modifiers | Sceneviewerinput.MODIFIER_FLAG_SHIFT

    return modifiers
# mapping from qt to zinc end

SELECTION_RUBBERBAND_NAME = 'selection_rubberband'


class ProjectionMode(object):

    PARALLEL = 0
    PERSPECTIVE = 1


class SelectionMode(object):

    NONE = -1
    EXCLUSIVE = 0
    ADDITIVE = 1
    CREATE = 2
    EDIT = 3


class SceneviewerWidget(QtOpenGL.QGLWidget):

    # Create a signal to notify when the sceneviewer is ready.
    graphicsInitialized = QtCore.Signal()
    graphicGroup = QtCore.Signal()
    mouseMoved = QtCore.Signal(float)

    # init start
    def __init__(self, parent=None, shared=None):
        """
        Call the super class init functions, set the  Zinc context and the scene viewer handle to None.
        Initialise other attributes that deal with selection and the rotation of the plane.
        """
        QtOpenGL.QGLWidget.__init__(self, parent, shared)
        # Create a Zinc context from which all other objects can be derived either directly or indirectly.
        self._context = None
        self._sceneviewer = None
        self._scenepicker = None

        # Selection attributes
        self._viewOnlyEvent = False
        self._selectionMode = "VIEW"
        self._selectionGroup = None
        self._selectionBox = None
        self._ignore_mouse_events = False
        self.setFocusPolicy(QtCore.Qt.StrongFocus)

        self._editModifier = QtCore.Qt.CTRL
        self._selectionModifier = QtCore.Qt.SHIFT
        self._additiveSelectionModifier = QtCore.Qt.ALT

        self._nodeConstrainMode = False
        self._zincTool = None

    def setSelectionMode(self, mode):
        self._selectionMode = mode

    def setContext(self, context):
        """
        Sets the context for this ZincWidget.  This should be set before the initializeGL()
        method is called otherwise the scene viewer cannot be created.
        """
        self._context = context

    def getContext(self):
        if not self._context is None:
            return self._context
        else:
            raise RuntimeError("Zinc context has not been set in Sceneviewerwidget.")

    def getSceneviewer(self):
        """
        Get the scene viewer for this ZincWidget.
        """
        return self._sceneviewer

    def setNodeCreateCoordinatesField(self, coordinate_field):
        self._createCoordinatesField = coordinate_field

    # initializeGL start
    def initializeGL(self):
        """
        Initialise the Zinc scene for drawing the axis glyph at a point.  
        """
        # Following throws exception if you haven't called setContext() yet
        self.getContext()
        if self._sceneviewer is None:
            # Get the scene viewer module.
            scene_viewer_module = self._context.getSceneviewermodule()

            # From the scene viewer module we can create a scene viewer, we set up the
            # scene viewer to have the same OpenGL properties as the QGLWidget.
            self._sceneviewer = scene_viewer_module.createSceneviewer(Sceneviewer.BUFFERING_MODE_DOUBLE, Sceneviewer.STEREO_MODE_DEFAULT)
            self._sceneviewer.setProjectionMode(Sceneviewer.PROJECTION_MODE_PERSPECTIVE)
            self._sceneviewer.setTransparencyMode(Sceneviewer.TRANSPARENCY_MODE_SLOW)
            # self._sceneviewer.setTransparencyMode(Sceneviewer.TRANSPARENCY_MODE_ORDER_INDEPENDENT)
            # self._sceneviewer.setTransparencyLayers(5)

            # Create a filter for visibility flags which will allow us to see our graphic.
            filter_module = self._context.getScenefiltermodule()
            # By default graphics are created with their visibility flags set to on (or true).
            graphics_filter = filter_module.createScenefilterVisibilityFlags()

            # Set the graphics filter for the scene viewer otherwise nothing will be visible.
            self._sceneviewer.setScenefilter(graphics_filter)
            region = self._context.getDefaultRegion()
            scene = region.getScene()
            fieldmodule = region.getFieldmodule()

            self._sceneviewer.setScene(scene)

            self._selectionGroup = fieldmodule.createFieldGroup()
            scene.setSelectionField(self._selectionGroup)

            self._scenepicker = scene.createScenepicker()
            self._scenepicker.setScenefilter(graphics_filter)
            
            if self._selectionBox:
                previousScene = self._selectionBox.getScene()
                previousScene.removeGraphics(self._selectionBox)
    
            self._selectionBox = scene.createGraphicsPoints()
            self._selectionBox.setName(SELECTION_RUBBERBAND_NAME)
            self._selectionBox.setScenecoordinatesystem(SCENECOORDINATESYSTEM_WINDOW_PIXEL_TOP_LEFT)
    
            attributes = self._selectionBox.getGraphicspointattributes()
            attributes.setGlyphShapeType(Glyph.SHAPE_TYPE_CUBE_WIREFRAME)
            attributes.setBaseSize([10, 10, 0.9999])
            attributes.setGlyphOffset([1, -1, 0])
            self._selection_box_setBaseSize = attributes.setBaseSize
            self._selection_box_setGlyphOffset = attributes.setGlyphOffset
            self._selectionBox.setVisibilityFlag(False)

            # Set up unproject pipeline
            # self._window_coords_from = fieldmodule.createFieldConstant([0, 0, 0])
            # self._global_coords_from = fieldmodule.createFieldConstant([0, 0, 0])
            # unproject = fieldmodule.createFieldSceneviewerProjection(self._sceneviewer, SCENECOORDINATESYSTEM_WINDOW_PIXEL_TOP_LEFT, SCENECOORDINATESYSTEM_WORLD)
            # project = fieldmodule.createFieldSceneviewerProjection(self._sceneviewer, SCENECOORDINATESYSTEM_WORLD, SCENECOORDINATESYSTEM_WINDOW_PIXEL_TOP_LEFT)

            # self._global_coords_to = fieldmodule.createFieldProjection(self._window_coords_from, unproject)
            # self._window_coords_to = fieldmodule.createFieldProjection(self._global_coords_from, project)

            self._sceneviewernotifier = self._sceneviewer.createSceneviewernotifier()
            self._sceneviewernotifier.setCallback(self._zincSceneviewerEvent)

            self.graphicsInitialized.emit()

            self._zincTool.setupInteraction(self._sceneviewer, self._scenepicker,
                                            fieldmodule.findFieldByName('coordinates'))

            self.graphicGroup.emit()

            self.viewAll()

    def setTool(self, tool):
        self._zincTool = tool

    def setProjectionMode(self, mode):
        if mode == ProjectionMode.PARALLEL:
            self._sceneviewer.setProjectionMode(Sceneviewer.PROJECTION_MODE_PARALLEL)
        elif mode == ProjectionMode.PERSPECTIVE:
            self._sceneviewer.setProjectionMode(Sceneviewer.PROJECTION_MODE_PERSPECTIVE)

    def getProjectionMode(self):
        if self._sceneviewer.getProjectionMode() == Sceneviewer.PROJECTION_MODE_PARALLEL:
            return ProjectionMode.PARALLEL
        elif self._sceneviewer.getProjectionMode() == Sceneviewer.PROJECTION_MODE_PERSPECTIVE:
            return ProjectionMode.PERSPECTIVE

    def getViewParameters(self):
        result, eye, lookat, up = self._sceneviewer.getLookatParameters()
        if result == OK:
            angle = self._sceneviewer.getViewAngle()
            return (eye, lookat, up, angle)

        return None

    def setViewParameters(self, eye, lookat, up, angle):
        self._sceneviewer.beginChange()
        self._sceneviewer.setLookatParametersNonSkew(eye, lookat, up)
        self._sceneviewer.setViewAngle(angle)
        self._sceneviewer.endChange()

    def setScenefilter(self, scenefilter):
        self._sceneviewer.setScenefilter(scenefilter)

    def getScenefilter(self):
        return self._sceneviewer.getScenefilter()

    def getScenepicker(self):
        return self._scenepicker

    def setScenepicker(self, scenepicker):
        self._scenepicker = scenepicker

    def setPickingRectangle(self, coordinate_system, left, bottom, right, top):
        self._scenepicker.setSceneviewerRectangle(self._sceneviewer, coordinate_system, left, bottom, right, top);

    def setSelectionfilter(self, scenefilter):
        self._scenepicker.setScenefilter(scenefilter)

    def getViewportSize(self):
        result, width, height = self._sceneviewer.getViewportSize()
        if result == OK:
            return width, height

        return None

    def setTumbleRate(self, rate):
        self._sceneviewer.setTumbleRate(rate)

    def setIgnoreMouseEvents(self, value):
        self._ignore_mouse_events = value

    def viewAll(self):
        """
        Helper method to set the current scene viewer to view everything
        visible in the current scene.
        """
        self._sceneviewer.viewAll()

    # paintGL start
    def paintGL(self):
        """
        Render the scene for this scene viewer.  The QGLWidget has already set up the
        correct OpenGL buffer for us so all we need do is render into it.  The scene viewer
        will clear the background so any OpenGL drawing of your own needs to go after this
        API call.
        """
        self._sceneviewer.renderScene()
        # paintGL end

    def _zincSceneviewerEvent(self, event):
        """
        Process a scene viewer event.  The updateGL() method is called for a
        repaint required event all other events are ignored.
        """
        if event.getChangeFlags() & Sceneviewerevent.CHANGE_FLAG_REPAINT_REQUIRED:
            QtCore.QTimer.singleShot(0, self.updateGL)

    def resizeGL(self, width, height):
        """ Respond to widget resize events. """
        self._sceneviewer.setViewportSize(width, height)

    def mousePressEvent(self, event):

        # Resets everything!
        self._selection_position_start = None
        self._handle_mouse_events = False
        self._viewOnlyEvent = False
        if self._ignore_mouse_events:
            return

        if self._selectionMode == "CREATE" and self._nonViewEvent(event):

            # If Left Click and Shift + Control. Store position else continue
            if self._nonViewEvent(event):
                self._selection_position_start = (event.x(), event.y())
                self._handle_mouse_events = True
                return

        elif self._selectionMode == "EDIT" and self._nonViewEvent(event):
            # Left Click needs to store the starting position but only if modifiered
            if self._nonViewEvent(event):
                pos = (event.x(), event.y())

                if event.modifiers() & QtCore.Qt.CTRL:
                    if not self._zincTool.nodeIsSelectedAtCoordinates(*pos).isValid():
                        return

                self._selection_position_start = pos
                self._zincTool.setMoveStart(pos)
                self._handle_mouse_events = True
                return

        scene_input = self._sceneviewer.createSceneviewerinput()
        scene_input.setPosition(event.x(), event.y())
        scene_input.setEventType(Sceneviewerinput.EVENT_TYPE_BUTTON_PRESS)
        scene_input.setButtonType(button_map[event.button()])
        scene_input.setModifierFlags(modifier_map(event.modifiers()))
        self._sceneviewer.processSceneviewerinput(scene_input)
        self._handle_mouse_events = True
        self._viewOnlyEvent = True

    def mouseMoveEvent(self, event):

        if self._ignore_mouse_events or not self._handle_mouse_events:
            return

        if self._selectionMode == "CREATE" and self._nonViewEvent(event):
            # TODO add if moved x amount.
            self._handle_mouse_events = False
            return

        elif self._selectionMode == "EDIT" and self._nonViewEvent(event):
            # If CTRL and something is selected that is not an element.
            if event.modifiers() & QtCore.Qt.CTRL:
                # Check for selection group
                if not self._selectionGroup.isEmpty():

                    fieldmodule = self._selectionGroup.getFieldmodule()
                    nodeset = fieldmodule.findNodesetByFieldDomainType(Field.DOMAIN_TYPE_NODES)
                    nodesetGroup = self._selectionGroup.getFieldNodeGroup(nodeset)
                    if nodesetGroup.isValid():
                        size = nodesetGroup.getNodesetGroup().getSize()
                        if size > 0:
                            pos = (event.x(), event.y())
                            self._zincTool.moveNodes(pos)
                            return
                        else:
                            self._handle_mouse_events = False
                    self._handle_mouse_events = False
                return

            # If SHIFT starts the selection thing.
            # Update the end location of the bandbox
            # Draws the bandbox
            elif event.modifiers() & QtCore.Qt.SHIFT:
                pos = (event.x(), event.y())
                self._selection_position_end = pos
                self._zincTool.drawBox(pos)
            return

        scene_input = self._sceneviewer.createSceneviewerinput()
        scene_input.setPosition(event.x(), event.y())
        scene_input.setEventType(Sceneviewerinput.EVENT_TYPE_MOTION_NOTIFY)
        if event.type() == QtCore.QEvent.Leave:
            scene_input.setPosition(-1, -1)
        self._sceneviewer.processSceneviewerinput(scene_input)
        self._viewOnlyEvent = True

    def mouseReleaseEvent(self, event):

        if self._ignore_mouse_events:
            return

        # If invalid move then just
        if self._handle_mouse_events:

            if self._selectionMode == "CREATE" and self._nonViewEvent(event):
                pos = (event.x(), event.y())
                self._selection_position_end = pos

                if event.modifiers() & QtCore.Qt.SHIFT:
                    # If SHIFT Select Nearest Graphic and add to selection
                    self._zincTool.createElement(pos)

                elif event.modifiers() & QtCore.Qt.CTRL:
                    # If CTRL Create a node if one doesn't exist
                    self._zincTool.createNode(pos)

                else:
                    print "This shouldn't be possible!!!"
                return

            elif self._selectionMode == "EDIT" and self._nonViewEvent(event):

                if event.modifiers() & QtCore.Qt.SHIFT:
                    # Checks if there is a big difference between start and current.
                    pos = (event.x(), event.y())
                    self._zincTool.selectGraphic(pos)
                    # Adds everything in the bandbox to the selection.

                elif event.modifiers() & QtCore.Qt.CTRL:
                    pos = (event.x(), event.y())
                    self._zincTool.moveNodes(pos)
                return

        scene_input = self._sceneviewer.createSceneviewerinput()
        scene_input.setPosition(event.x(), event.y())
        scene_input.setEventType(Sceneviewerinput.EVENT_TYPE_BUTTON_RELEASE)
        scene_input.setButtonType(button_map[event.button()])
        self._sceneviewer.processSceneviewerinput(scene_input)

    def _nonViewEvent(self, event):
        """ Helper function that is commonly used """
        return event.modifiers() & QtCore.Qt.SHIFT or event.modifiers() & QtCore.Qt.CTRL and not self._viewOnlyEvent

    def wheelEvent(self, event):
        numDegrees = event.delta() / 8
        numSteps = numDegrees / 15

        if event.orientation() == QtCore.Qt.Vertical:
            self.mouseMoved.emit(numSteps * 5.0)
        else:
            event.ignore()

        event.accept()