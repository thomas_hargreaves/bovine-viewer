"""
Contains the code that changes the nodes
"""

from opencmiss.zinc.field import Field
from opencmiss.zinc.glyph import Glyph

from base_class import Base
from zinc.zinc_helper import *


class Nodes(Base):

    # static constants
    FILENAME = "created_nodes.exnode"
    GROUP_NAME = "CreatedNodes"

    def __init__(self, context, sceneviewer):
        super(Nodes, self).__init__(context)
        self._sceneviewer = sceneviewer
        self._depthFields = None
        self._coincidentGraphics = None
        self._planeDepthField = None

    def setCoordinateField(self, coordinateField):
        self._sceneviewer.setNodeCreateCoordinatesField(coordinateField)

    def createNodeGraphics(self):
        region = self._context.getDefaultRegion()

        with get_materialmodule(region) as material_module:
            blue_material = material_module.findMaterialByName('blue')
            gold_material = material_module.findMaterialByName('gold')

        with get_fieldmodule(region) as fieldmodule:
            finite_element_field = fieldmodule.findFieldByName('coordinates')
            # group = fieldmodule.findFieldByName('skeleton')
            plane_group = fieldmodule.findFieldByName('plane')
            # and_group = fieldmodule.createFieldOr(group, plane_group)
            not_group = fieldmodule.createFieldNot(plane_group)
            node_name = fieldmodule.findFieldByName('selection_number')

            # Create the three fields that will be defined as the current nodes.
            x_comp = fieldmodule.createFieldComponent(finite_element_field, 1)
            y_comp = fieldmodule.createFieldComponent(finite_element_field, 2)
            z_comp = fieldmodule.createFieldComponent(finite_element_field, 3)
            plane_distance = fieldmodule.createFieldConstant(0.0)
            plane_distance.setManaged(True)

            tolerance_field = fieldmodule.createFieldConstant(1e-4)

            # Replace with a % tolerance and make sure the four plane nodes are not affected.
            x_field = fieldmodule.createFieldSubtract(x_comp, plane_distance)
            x_field = fieldmodule.createFieldAbs(x_field)
            x_field = fieldmodule.createFieldLessThan(x_field, tolerance_field)
            x_field = fieldmodule.createFieldAnd(x_field, not_group)
            x_field.setManaged(True)

            y_field = fieldmodule.createFieldSubtract(y_comp, plane_distance)
            y_field = fieldmodule.createFieldAbs(y_field)
            y_field = fieldmodule.createFieldLessThan(y_field, tolerance_field)
            y_field = fieldmodule.createFieldAnd(y_field, not_group)
            y_field.setManaged(True)

            z_field = fieldmodule.createFieldSubtract(z_comp, plane_distance)
            z_field = fieldmodule.createFieldAbs(z_field)
            z_field = fieldmodule.createFieldLessThan(z_field, tolerance_field)
            z_field = fieldmodule.createFieldAnd(z_field, not_group)
            z_field.setManaged(True)

            self._depthFields = [x_field, y_field, z_field]
            self._planeDepthField = plane_distance

        with get_scene(region) as scene:
            points = scene.createGraphicsPoints()
            points.setCoordinateField(finite_element_field)
            points.setMaterial(blue_material)
            points.setFieldDomainType(Field.DOMAIN_TYPE_NODES)
            points.setSubgroupField(not_group)

            attributes = points.getGraphicspointattributes()
            attributes.setBaseSize(0.002)
            attributes.setGlyphShapeType(Glyph.SHAPE_TYPE_SPHERE)
            attributes.setLabelField(node_name)
            # attributes.setLabelOffset([0.5, 0.5, 0.5])

            # Create node graphics that are on the plane.
            # They will have the same x, y or z coordinates as the current plane location
            points = scene.createGraphicsPoints()
            points.setCoordinateField(finite_element_field)
            points.setMaterial(gold_material)
            points.setFieldDomainType(Field.DOMAIN_TYPE_NODES)
            points.setSubgroupField(x_field)
            self._coincidentGraphics = points

            attributes = points.getGraphicspointattributes()
            attributes.setBaseSize(0.005)
            attributes.setGlyphShapeType(Glyph.SHAPE_TYPE_SPHERE)

    def changeDepthField(self, n):
        self._coincidentGraphics.setSubgroupField(self._depthFields[n])

    def updateDepth(self, depth):
        with get_fieldmodule(self._context.getDefaultRegion()) as fieldmodule:
            fieldcache = fieldmodule.createFieldcache()
            self._planeDepthField.assignReal(fieldcache, depth)

    def createGroupNodeGraphics(self):
        region = self._context.getDefaultRegion()

        with get_materialmodule(region) as material_module:
            cyan_material = material_module.findMaterialByName('cyan')

        with get_fieldmodule(region) as fieldmodule:
            finite_element_field = fieldmodule.findFieldByName('coordinates')
            group = fieldmodule.findFieldByName(Nodes.GROUP_NAME)
            plane_group = fieldmodule.findFieldByName('plane')
            not_group = fieldmodule.createFieldNot(plane_group)
            group = fieldmodule.createFieldAnd(not_group, group)

        with get_scene(region) as scene:
            # Create graphics for the nodes that are only created nodes and nothing else
            points = scene.createGraphicsPoints()
            points.setCoordinateField(finite_element_field)
            points.setMaterial(cyan_material)
            points.setFieldDomainType(Field.DOMAIN_TYPE_NODES)
            points.setSubgroupField(group)

            attributes = points.getGraphicspointattributes()
            attributes.setBaseSize(0.004)
            attributes.setGlyphShapeType(Glyph.SHAPE_TYPE_SPHERE)
