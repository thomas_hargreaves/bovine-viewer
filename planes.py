import sys

from PySide import QtCore
from opencmiss.zinc.element import Element, Elementbasis
from opencmiss.zinc.field import Field
from opencmiss.zinc.graphics import Graphics
from opencmiss.zinc.material import Material
from opencmiss.zinc.status import *

from base_class import Base
from external_lib.noconflict import classmaker
from zinc.zinc_helper import get_fieldmodule, get_scene, get_materialmodule


class Planes(Base, QtCore.QObject):

    __metaclass__ = classmaker()

    changeCoincidentGraphics = QtCore.Signal(int)
    updatePlaneDepth = QtCore.Signal(float)

    def __init__(self, context):
        Base.__init__(self, context)
        QtCore.QObject.__init__(self)

        self._nodes = []
        self._elements = []
        self._limits = []
        self._currentPosition = []
        self._currentPlane = 0
        self._element = None

    def createFiniteElements(self):
        """
        This function gets the maximum and minimum values of the nodeset and creates three planes.
        :return:
        """

        region = self._context.getDefaultRegion()

        # find the limits of the skeleton region and set it as the plane limits.
        with get_fieldmodule(region.findChildByName('skeleton')) as fieldmodule:

            nodeset = fieldmodule.findNodesetByFieldDomainType(Field.DOMAIN_TYPE_NODES)
            finite_element_field = fieldmodule.findFieldByName('coordinates')

            if finite_element_field.isValid():

                minField = fieldmodule.createFieldNodesetMinimum(finite_element_field, nodeset)
                maxField = fieldmodule.createFieldNodesetMaximum(finite_element_field, nodeset)

                fieldcache = fieldmodule.createFieldcache()
                status, minCoord = minField.evaluateReal(fieldcache, 3)
                status, maxCoord = maxField.evaluateReal(fieldcache, 3)

                if status == OK:

                    # Add an additional 5% to the nodesets
                    distance = [maxCoord[i] - minCoord[i] for i in range(3)]
                    for i, value in enumerate(minCoord):
                        if value > 0:
                            minCoord[i] = value + distance[i] * 0.05
                        else:
                            minCoord[i] = value - distance[i] * 0.05

                    for i, value in enumerate(maxCoord):
                        if value > 0:
                            maxCoord[i] = value + distance[i] * 0.05
                        else:
                            maxCoord[i] = value - distance[i] * 0.05

                    distance = [maxCoord[i] - minCoord[i] for i in range(3)]
                    self._limits = zip(minCoord, maxCoord)
                    Result = []

                    # Generates the extreme points of the mid points.
                    combinations = [
                        [0.5, 0, 0], [0.5, 1, 1],
                        [0, 0.5, 0], [1, 0.5, 1],
                        [0, 0, 0.5], [1, 1, 0.5],
                    ]

                    for combination in combinations:
                        value = [(minCoord[i] + distance[i] * combination[i]) for i in range(3)]
                        Result.append(value)

                    for i in range(3):
                        self._currentPosition.append(self.generatePoints(*Result[2 * i:2 * i + 2]))

        with get_fieldmodule(region) as fieldmodule:

            nodeset = fieldmodule.findNodesetByFieldDomainType(Field.DOMAIN_TYPE_NODES)
            mesh = fieldmodule.findMeshByDimension(2)

            # Creates the plane group
            plane_group = fieldmodule.createFieldGroup()
            plane_group.setName('plane')
            plane_group.setManaged(True)
            node_group = plane_group.createFieldNodeGroup(nodeset)
            element_group = plane_group.createFieldElementGroup(mesh)

            finite_element_field = fieldmodule.findFieldByName('coordinates')
            if not finite_element_field.isValid():
                finite_element_field = fieldmodule.createFieldFiniteElement(3)
                finite_element_field.setName('coordinates')
                finite_element_field.setManaged(True)
                finite_element_field.setTypeCoordinate(True)

            # Finds the two extreme points of the nodeset and finds the midpoints on the corresponding rectangle
            if finite_element_field.isValid():

                self.create2DFiniteElement(fieldmodule, finite_element_field, self._currentPosition[0], 'plane')

                # Gets the three elements and saves them.
                meshGroup = element_group.getMeshGroup()
                element = meshGroup.createElementiterator().next()
                self._element = element
                nodesetGroup = node_group.getNodesetGroup()
                nodeIter = nodesetGroup.createNodeiterator()
                for n in range(4):
                    node = nodeIter.next()
                    self._nodes.append(node)

                with get_materialmodule(self._context) as materialmodule:
                    # green_matieral = materialmodule.findMaterialByName('green')
                    # green_matieral.setAttributeReal(Material.ATTRIBUTE_ALPHA, 0.6)
                    green_matieral = materialmodule.createMaterial()
                    colour = [0.2, 0.8, 0.2]
                    green_matieral.setAttributeReal3(Material.ATTRIBUTE_AMBIENT, colour)
                    green_matieral.setAttributeReal3(Material.ATTRIBUTE_DIFFUSE, colour)
                    green_matieral.setAttributeReal3(Material.ATTRIBUTE_EMISSION, colour)
                    green_matieral.setAttributeReal3(Material.ATTRIBUTE_SPECULAR, colour)
                    green_matieral.setAttributeReal(Material.ATTRIBUTE_SHININESS, 1)

                with get_scene(region) as scene:
                    surface = scene.createGraphicsSurfaces()
                    surface.setCoordinateField(finite_element_field)
                    surface.setName('plane')
                    surface.setSubgroupField(plane_group)
                    surface.setMaterial(green_matieral)
                    surface.setSelectMode(Graphics.SELECT_MODE_OFF)
                    surface.setVisibilityFlag(False)

    def generatePoints(self, a, b):
        """
        Generates the four points from two points.
        :param a: Counted as the lesser
        :param b: Counted as the greater
        :return: Four coordinates of node plane.
        """

        index = -1
        coordinates = [a]

        for i, pair in enumerate(zip(a, b)):
            if abs(pair[0] - pair[1]) < sys.float_info.epsilon:
                index = i
            else:
                continue

        if index == -1:
            raise ArithmeticError("Error with coordinates")

        diff = [b[i] - a[i] for i in range(3) if i != index]
        diff.insert(index, 0.0)

        for j in range(3):
            if j == index:
                continue
            temp = list(a)
            temp[j] += diff[j]
            coordinates.append(temp)
        coordinates.append(b)

        return coordinates

    def changePlane(self, number):
        """
        This method changes the current plane. It first shows the current value.
        :return: None
        """

        region = self._context.getDefaultRegion()

        # Hid the graphics
        with get_scene(region) as scene:
            graphics = scene.findGraphicsByName('plane')
            if number == 3:
                graphics.setVisibilityFlag(False)
                return
            else:
                graphics.setVisibilityFlag(True)

        # Saves the coordinates into an array
        with get_fieldmodule(region) as fieldmodule:

            fininte_element_field = fieldmodule.findFieldByName('coordinates')
            fieldcache = fieldmodule.createFieldcache()

            for n, node in enumerate(self._nodes):
                if node.isValid():
                    fieldcache.setNode(node)
                    status, coords = fininte_element_field.evaluateReal(fieldcache, 3)
                    if status != OK:
                        raise StandardError("Error evaluating coordinates at the nodes")

                    # Replace with the new coordinates
                    status = fininte_element_field.assignReal(fieldcache, self._currentPosition[number][n])
                    if status != OK:
                        raise StandardError("Error assigning coordinates to the nodes")
                    self._currentPosition[self._currentPlane][n] = coords

        # Finds the nodeset that is in plane and updates the coordinates to the saved for the dimension
        self._currentPlane = number
        self.changeCoincidentGraphics.emit(number)
        self.updatePlaneDepth.emit(self._currentPosition[number][0][number])

    def moveCurrentPlane(self, percentage):
        """
        From the slider move the plane.
        :param percentage: Int from 0-100
        :return: None
        """

        region = self._context.getDefaultRegion()

        index = self._currentPlane
        limit = self._limits[index]
        new_coordinate = (limit[1] - limit[0]) * percentage + limit[0]
        coordinates = self._currentPosition[index]

        with get_fieldmodule(region) as fieldmodule:

            finite_element_field = fieldmodule.findFieldByName('coordinates')
            fieldcache = fieldmodule.createFieldcache()

            for n, node in enumerate(self._nodes):
                if node.isValid():
                    fieldcache.setNode(node)
                    coordinates[n][index] = new_coordinate

                    status = finite_element_field.assignReal(fieldcache, coordinates[n])
                    if status != OK:
                        raise StandardError("Error assigning coordinates")

        self.updatePlaneDepth.emit(new_coordinate)

    def setCurrentPosition(self, n, pos):
        self._currentPosition[n] = pos

    def getPlaneElement(self):
        return self._element

    @staticmethod
    def create2DFiniteElement(fieldmodule, finite_element_field, node_coordinates, group=None):
        """
        Create a 2D finite element from the fieldmodule, finite_element_field and node_coordinates.
        :param fieldmodule: The field module
        :param finite_element_field: The coordinate field to add the nodes into and set their location
        :param node_coordinates: The four sets of coordinates to create nodes at
        :param group: The group to
        """

        nodeset = fieldmodule.findNodesetByName('nodes')
        mesh = fieldmodule.findMeshByDimension(2)
        nodeset_group = None
        mesh_group = None

        if group is not None:
            group = fieldmodule.findFieldByName(group).castGroup()

            nodeset_group = group.getFieldNodeGroup(nodeset).getNodesetGroup()
            if not nodeset_group.isValid():
                nodeset_group = group.createFieldNodeGroup(nodeset)
                nodeset_group.setManaged(True)
                nodeset_group = nodeset_group.getNodesetGroup()
            mesh_group = group.getFieldElementGroup(mesh).getMeshGroup()
            if not mesh_group.isValid():
                mesh_group = group.createFieldElementGroup(mesh)
                mesh_group.setManaged(True)
                mesh_group = mesh_group.getMeshGroup()

        node_template = nodeset.createNodetemplate()
        node_template.defineField(finite_element_field)

        field_cache = fieldmodule.createFieldcache()

        node_identifiers = []
        for node_coordinate in node_coordinates:
            node = nodeset.createNode(-1, node_template)
            node_identifiers.append(node.getIdentifier())
            field_cache.setNode(node)
            finite_element_field.assignReal(field_cache, node_coordinate)
            if group is not None:
                nodeset_group.addNode(node)

        element_template = mesh.createElementtemplate()
        element_template.setElementShapeType(Element.SHAPE_TYPE_SQUARE)
        element_node_count = 4
        element_template.setNumberOfNodes(element_node_count)
        linear_basis = fieldmodule.createElementbasis(2, Elementbasis.FUNCTION_TYPE_LINEAR_LAGRANGE)
        node_indexes = [1, 2, 3, 4]
        element_template.defineFieldSimpleNodal(finite_element_field, -1, linear_basis, node_indexes)

        for i, node_identifier in enumerate(node_identifiers):
            node = nodeset.findNodeByIdentifier(node_identifier)
            element_template.setNode(i + 1, node)

        if group is not None:
            element = mesh.createElement(-1, element_template)
            mesh_group.addElement(element)
        else:
            mesh.defineElement(-1, element_template)

