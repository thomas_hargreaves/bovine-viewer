"""
This is the base class that contains methods that are used through out the helper classes
It is an abstract base class
"""

import abc


class Base(object):

    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def __init__(self, context):
        self._context = context
        self._sceneviewer = None
        self._tool = None

    def getSceneviewer(self):
        return self._sceneviewer

    def setTool(self, tool):
        self._tool = tool

    def getTool(self):
        return self._tool