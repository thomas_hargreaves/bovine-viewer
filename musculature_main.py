from opencmiss.zinc.context import Context
from PySide import QtGui, QtCore
from UI.viewer_widget import ViewerWidget
import sys


"""
This is the main program. It creates the context and displays the GUI. It isn't a class. Not sure if this is a problem
Creates the context, GUI. It checks to make sure everything isn't None and then runs the GUI.
"""

context = Context('Musculature creator')
context.getMaterialmodule().defineStandardMaterials()
context.getGlyphmodule().defineStandardGlyphs()

app = QtGui.QApplication(sys.argv)

e = ViewerWidget(context)
e.show()
sys.exit(app.exec_())






