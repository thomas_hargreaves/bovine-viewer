"""
This class helps load files into a region but adds functionality to change region/groups by using a temp region
"""

import os
import random
import tempfile

from opencmiss.zinc.field import Field
from opencmiss.zinc.region import Region

from zinc.zinc_helper import *


class LoadExfile(object):

    def __init__(self, region, filenames):
        """
         @:param region: The region instance that the files will be loaded into
         @:param filenames: List containing all the
         @:param regionPath: String of the region path
         @:param groupName: The group to be listed into
        """

        if isinstance(region, Region) and region.isValid():
            self._region = region
        else:
            raise ValueError("Invalid region")

        if isinstance(filenames, list):
            self._filenames = filenames

        # Load the filenames into a temporary region and figure out how many groups there are!
        # Creates a random region to temporarily load in the file!
        region_name = "temp region#{}".format(random.randint(0, 999999))
        while region.findChildByName(region_name).isValid():
            region_name = "temp region#{}".format(random.randint(0, 999999))

        # Create the streaminformation Region and load in the files
        self._tempRegion = region.createChild(region_name)
        with region_change(self._tempRegion):
            region_stream = self._tempRegion.createStreaminformationRegion()
            if not isinstance(filenames, list):
                filenames = [filenames]
            for filename in filenames:
                region_stream.createStreamresourceFile(str(filename))
            self._tempRegion.read(region_stream)

        # Figure out the Region with all the nodes in it.
        self._tempRegion = self._getNonEmptyRegion(self._tempRegion)

        # Figure out the Group and save!
        self._groupName = ""
        with get_fieldmodule(self._tempRegion) as fieldmodule:
            fielditerator = fieldmodule.createFielditerator()
            field = fielditerator.next()

            while field.isValid():
                if field.castGroup().isValid():
                    field.castGroup().clearLocal()
                    field.setManaged(False)
                    break
                field = fielditerator.next()

    def loadFile(self, regionPath="", fieldGroupName=""):
        """"
        Depending on the state of the object the thing works
        Checks if the region exists, else create it.
        Loads the file into a fake
        Figures out the field group
        """

        if isinstance(regionPath, str):
            self._setUpRegionPath(regionPath)

        # if isinstance(fieldGroupName, str):
        #     self._targetGroupName = fieldGroupName

        # Import the group names
        if fieldGroupName:
            with get_fieldmodule(self._tempRegion) as fieldmodule:

                nodeset = fieldmodule.findNodesetByFieldDomainType(Field.DOMAIN_TYPE_NODES)
                entire_domain = fieldmodule.createFieldConstant(1.0)

                fieldGroup = fieldmodule.createFieldGroup()
                fieldGroup.setName(fieldGroupName)
                fieldGroup.setManaged(True)

                nodesetGroup = fieldGroup.createFieldNodeGroup(nodeset).getNodesetGroup()
                nodesetGroup.addNodesConditional(entire_domain)

                for dim in range(1, 4):
                    mesh = fieldmodule.findMeshByDimension(dim)
                    meshGroup = fieldGroup.createFieldElementGroup(mesh).getMeshGroup()
                    meshGroup.addElementsConditional(entire_domain)

        # Write to file
        fd, temp_path = tempfile.mkstemp()

        temp_path = "./temptemp.exformat"

        try:
            self._tempRegion.beginChange()
            streaminformation = self._tempRegion.createStreaminformationRegion()
            streaminformation.createStreamresourceFile(temp_path)
            self._tempRegion.write(streaminformation)

            readStreaminformation = self._targetRegion.createStreaminformationRegion()
            readStreaminformation.createStreamresourceFile(temp_path)
            self._targetRegion.read(readStreaminformation)
        finally:
            os.close(fd)
            if os.path.isfile(temp_path):
                os.remove(temp_path)
            self._tempRegion.endChange()
            self._region.beginChange()
            self._region.removeChild(self._tempRegion)
            self._region.endChange()

    def _setUpRegionPath(self, regionPath):
        # Checks if the region path is valid
        # Then sets up the

        # Empty regionPath
        if regionPath is None:
            self._targetRegion = self._region
            return

        self._region.beginChange()
        region = self._region.findSubregionAtPath(regionPath)
        if not region.isValid():
            region = self._region.createSubregion(regionPath)

        # Set target region to be the newly created region
        self._targetRegion = region
        self._region.endChange()

    def _getNonEmptyRegion(self, region):
        # From an input region it checks and finds the first non-empty region

        if not region or not region.isValid():
            raise ValueError("Invalid region")

        size = self._getRegionSize(region)
        if size != 0:
            return region

        region_list = [region]

        # Check other regions for the nodes
        while size == 0:
            current = region.getFirstChild()
            while current.isValid():
                region_list.append(current)
                current = current.getNextSibling()

            # TODO catch the empty region exception
            try:
                region_temp = region_list.pop()
                size = self._getRegionSize(region_temp)
                region = region_temp
            except IndexError:
                # Don't set the region to the temp_region
                return region

        return region

    def _getRegionSize(self, region):
        return region.getFieldmodule().findNodesetByFieldDomainType(Field.DOMAIN_TYPE_NODES).getSize()