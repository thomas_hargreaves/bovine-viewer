"""
This is the main file for the .stl to .exfile converter. It is a stand alone program
"""

import sys

from PySide.QtGui import QFileDialog, QWidget, QApplication

import load_stl


# Get the file
# Get the save name


class Main(QWidget):
    def load(self):
        open_file, filter_used = QFileDialog.getOpenFileName(self, 'Please select the .stl file',
                                                              filter='STL ( *.stl )')
        if open_file and filter_used:
            save_name, filter_used = QFileDialog.getSaveFileName(self, 'Please enter the name to save the file',
                                                                  filter='Exformat ( *.exnode *.exelem )')
            if save_name and filter_used:
                load_stl.convertSTL(open_file, save_name)
                return

        self.close()

    def __int__(self):
        super(Main, self).__init__()

app = QApplication(sys.argv)
e = Main()
e.load()
sys.exit(app.exec_())

